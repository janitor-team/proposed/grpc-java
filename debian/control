Source: grpc-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Olek Wojnar <olek@debian.org>,
           Andreas Tille <tille@debian.org>,
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               gem2deb,
               gradle-apt-plugin,
               gradle-debian-helper,
               gradle-plugin-protobuf,
               javahelper,
               jdupes,
               libanimal-sniffer-java,
               libc-ares-dev,
               liberror-prone-java,
               libgeronimo-annotation-1.3-spec-java,
               libgflags-dev,
               libgoogle-auth-java (>= 0.18.0~),
               libgoogle-common-protos-java,
               libgoogle-gson-java,
               libgoogle-perftools-dev,
               libgradle-plugins-java,
               libgtest-dev,
               libguava-java (>= 28.1~),
               libjacoco-java,
               libjsr305-java,
               libnetty-java (>= 4.1.42~),
               libopencensus-java (>= 0.24.0~),
               libperfmark-java (>= 0.19.0~),
               libprotobuf-dev,
               libprotobuf-java,
               libprotoc-dev (>= 3.11.4~),
               libssl-dev,
               maven-repo-helper,
               protobuf-compiler (>= 3.11.4~),
               protobuf-compiler-grpc,
               python3,
               ruby-google-protobuf (>= 3.9.1~),
               ruby-googleapis-common-protos-types,
               ruby-googleauth (>= 0.5.1~),
               ruby-rspec,
               ruby-simplecov,
               zlib1g-dev,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/java-team/grpc-java
Vcs-Git: https://salsa.debian.org/java-team/grpc-java.git
Homepage: https://github.com/grpc/grpc-java

Package: libgrpc-java
Architecture: all
Multi-Arch: foreign
Depends: libopencensus-java (>= 0.24.0~), ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: Java gRPC implementation, HTTP/2 based RPC
 gRPC-Java works with JDK 7. gRPC-Java clients are supported on Android API
 levels 14 and up (Ice Cream Sandwich and later).

Package: protobuf-compiler-grpc-java-plugin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: high performance general RPC framework - protobuf Java plugin
 A modern, open source remote procedure call (RPC) framework that can run
 anywhere. It enables client and server applications to communicate
 transparently, and makes it easier to build connected systems.
 .
 This package provides the Java plugin needed for compiling gRPC service
 definitions with the protobuf compiler.
